## How to create HTML ready for email

You want to create email containing rich text and tables.  
Here is python code to do it assuming:
+ The text is written in markdown, possibily augmented by some html span elements.  
+ The tables are built from pandas dataframes.  

The [notebook](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/make-email-html-from-markdown-and-pandas/raw/master/demo_df_to_html.ipynb) shows how to build stand alone html pages
+ res1.html: [Bootstrap](https://v4-alpha.getbootstrap.com/content/tables/) styling
+ res2.html: [datatables.net](https://datatables.net/examples/basic_init/zero_configuration.html) styling and animations.
+ res3.html: Manual styling very close to native dataframe rendering in notebook (picture below), leveraging [dataframe styling](http://pandas.pydata.org/pandas-docs/stable/style.html).

<img src="img/styled_dataframe_snapshot.png" alt="Drawing" style="width: 300px;"/>

Now to go from standard HTML to 'Email HTML', there are many hurdles
+ Email HTML is [very special](https://www.sitepoint.com/how-to-code-html-email-newsletters/) and only a [subset of modern CSS is valid](https://www.campaignmonitor.com/css/). 
+ All css must be inline. For that we use [premailer](https://github.com/peterbe/premailer).

But it's difficult to get the result you want.  
Some people have [tried and struggled](http://stackoverflow.com/questions/36897366/pandas-to-html-using-the-style-options-or-custom-css).  

A convenient solution, though utimately manual, is to use the browser copy/paste.  
It does **a lot** of work under the hood to transform your page HTML+CSS into HTML+CSS that is understood by the email client.  



